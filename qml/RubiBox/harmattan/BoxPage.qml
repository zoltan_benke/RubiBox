// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "Component"
import "../scripts/createobject.js" as ObjectCreator
import "../scripts/const.js" as Const
import "../scripts/utils.js" as Utils
import "FastScroll"
import "Delegates"
import "Buttons"
import "ToolBars"

DevPDA{
    id: root


    function showCreateFolderDialog(){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/CreateFolder.qml"), root.pageStack)
        dialog.open()
    }


    function showRenameDialog(){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/RenameItem.qml"), root.pageStack)
        dialog.open()
    }


    function showRemoveDialog(){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/RemoveItem.qml"), root.pageStack)
        dialog.open()
    }


    function showAccountInfo(){
        root.pageStack.push(Qt.resolvedUrl("BoxAccountInfoPage.qml"))
    }


    function getCopyToolBar(){
        var toolbar = ObjectCreator.createObject(Qt.resolvedUrl("ToolBars/CopyBar.qml"), root.pageStack)
        return toolbar;
    }


    function getMoveToolBar(){
        var toolbar = ObjectCreator.createObject(Qt.resolvedUrl("ToolBars/MoveBar.qml"), root.pageStack)
        return toolbar;
    }


    function downloadSelected(){
        Session.downloadSelected()
        _deselectAllFiles()
    }


    Connections{
        target: Session
        onActionChanged:{
            if (Session.action === "normal"){
                root.pageStack.toolBar.setTools(normalBar, "replace")
                console.debug("NORMAL ACTION");
            }
            else if (Session.action === "copy"){
                root.pageStack.toolBar.setTools(getCopyToolBar(), "replace")
                console.debug("COPY ACTION");
            }
            else if (Session.action === "move"){
                root.pageStack.toolBar.setTools(getMoveToolBar(), "replace")
                console.debug("MOVE ACTION");
            }
        }
    }

    Connections{
        target: Box
        onAllItemsLoaded: {
            list.footerVisible = false
            infoBanner.parent = pageStack
            infoBanner.text = qsTr("All items loaded");
            infoBanner.show()
        }
        onRefreshTokenDone: {
            if (Database.setToken(Database.active_account, token, refresh_token)){
                Session.listFolder();
            }
        }
        onItemsDone: if (Session.action !== "normal") checkConflict(Utils.CM_ITEM_NAME)
    }


    tools: ToolBarLayout{
        id: normalBar
        visible: (Session.action === "normal")
        ToolIcon{
            platformIconId: Session.isRoot ? "toolbar-close" : "toolbar-up"
            onClicked: {
                if (Session.isRoot)
                    Qt.quit()
                else{
                    Session.listFolder(Session.parentFolder)
                }
            }
            onPressAndHold: {
                if (!Session.isRoot){
                    Session.listFolder()
                }
            }
        }
        ToolIcon{
            platformIconId: "toolbar-callhistory"
            onClicked: {
                root.pageStack.push(boxTransferPage)
            }
            CountBubble {
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.topMargin: -5
                value: TModel.count
                largeSized: true
                scale: value > 0 ? 1.0 : 0.0
                Behavior on scale { NumberAnimation{duration:  1000; easing.type: Easing.OutElastic} }
            }
        }
        ToolIcon{
            platformIconId: "toolbar-settings"
            onClicked: root.pageStack.push(Qt.resolvedUrl("BoxSettingsPage.qml"))
        }
        ToolIcon{
            iconSource: {
                var icon;
                if (Settings.inverted){
                    icon = Settings.multi_selection ? "Images/toolbar-multisel-white.png" : "Images/toolbar-singlesel-white.png";
                    return icon;
                }else{
                    icon = Settings.multi_selection ? "Images/toolbar-multisel.png" : "Images/toolbar-singlesel.png";
                }
            }
            onClicked: {
                if (Settings.multi_selection){
                    Settings.multi_selection = false;
                }else{
                    Settings.multi_selection = true
                }
            }
        }
        ToolIcon{
            platformIconId: "toolbar-add"
            onClicked: root.pageStack.push(fileBrowserPage)
        }
        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: (menu.status === DialogStatus.Open) ? menu.close() : menu.open()
        }
    }


    Menu{
        id: menu
        MenuLayout{
            MenuItem{text: qsTr("Create folder"); onClicked: {menu.close(); showCreateFolderDialog()}}
            MenuItem{text: qsTr("Account Info"); onClicked: {menu.close(); showAccountInfo()}}
        }
    }


    Menu{
        id: cmMenu
        MenuLayout{
            MenuItem{
                text: qsTr("%1 here").arg((Session.action === "move") ? qsTr("Move") : qsTr("Copy"));
                onClicked: {
                    menu.close()
                    Session.moveItem(Utils.CM_ITEM_ISDIR, Utils.CM_ITEM_ID)
                }
            }
        }
    }


    Menu{
        id: menuSelected
        MenuLayout{
            MenuItem{text: qsTr("Download selected"); onClicked: downloadSelected()}
        }
    }


    Menu{
        id: menuLongPressFile

        property bool checkLink

        MenuLayout{
            MenuItem{text: qsTr("Copy"); onClicked:
                {
                    menu.close();
                    Utils.CM_FOLDER_ID = Session.currentFolder
                    Utils.CM_ITEM_ID = getCurrentItem().id
                    Utils.CM_ITEM_NAME = getCurrentItem().name
                    Session.action = "copy"
                }
            }
            MenuItem{text: qsTr("Move"); onClicked:
                {
                    menu.close();
                    Utils.CM_FOLDER_ID = Session.currentFolder
                    Utils.CM_ITEM_ID = getCurrentItem().id
                    Utils.CM_ITEM_NAME = getCurrentItem().name
                    Session.action = "move"
                }
            }
            MenuItem{text: qsTr("Rename"); onClicked: {menu.close(); showRenameDialog()}}
            MenuItem{text: qsTr("Remove"); onClicked: {menu.close(); showRemoveDialog()}}
            MenuItem{
                text: qsTr("%1").arg(menuLongPressFile.checkLink ? "Get link" : "Share link")
                onClicked: {
                    if (menuLongPressFile.checkLink){
                        Session.share();
                    }else{
                        showLinkShareDialog(getCurrentItem().share_link)
                        console.debug(getCurrentItem().share_link);
                    }
                }
            }

            MenuItem{text: qsTr("Download"); onClicked: {menu.close();Session.downloadItem(getCurrentItem().id, getCurrentItem().name, getCurrentItem().size)}}
        }
    }

    Menu{
        id: menuLongPressDir

        property bool checkLink
        property bool isSynced: false

        MenuLayout{
            MenuItem{text: qsTr("Copy"); onClicked:
                {
                    menu.close();
                    Utils.CM_FOLDER_ID = Session.currentFolder
                    Utils.CM_ITEM_ID = getCurrentItem().id
                    Utils.CM_ITEM_NAME = getCurrentItem().name
                    Session.action = "copy"
                }
            }
            MenuItem{text: qsTr("Move"); onClicked:
                {
                    menu.close();
                    Utils.CM_FOLDER_ID = Session.currentFolder
                    Utils.CM_ITEM_ID = getCurrentItem().id
                    Utils.CM_ITEM_NAME = getCurrentItem().name
                    Session.action = "move"
                }
            }
            MenuItem{text: qsTr("Rename"); onClicked: {menu.close(); showRenameDialog()}}
            MenuItem{text: qsTr("Remove"); onClicked: {menu.close(); showRemoveDialog()}}
            MenuItem{
                text: menuLongPressDir.isSynced ? qsTr("Unsync") : qsTr("Sync")
                onClicked: Session.syncState(getCurrentItem().id, menuLongPressDir.isSynced ? "not_synced" : "synced")
            }
            MenuItem{
                text: qsTr("%1").arg(menuLongPressDir.checkLink ? "Get link" : "Share link")
                onClicked: {
                    if (menuLongPressDir.checkLink){
                        Session.share();
                    }else{
                        showLinkShareDialog(getCurrentItem().share_link)
                        console.debug(getCurrentItem().share_link);
                    }
                }
            }
        }
    }


    devTextTop: Session.currentHeader
    devText: Session.parentHeader
    onStatusChanged: {
        if (status === PageStatus.Active && !BModel.count){
        }
    }


    BoxTransferPage{
        id: boxTransferPage
    }

    BoxFileBrowserPage{
        id: fileBrowserPage
    }


    Label{
        id: noItems
        opacity: !Session.loading && !BModel.count ? 1 : 0
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: parent.height/2
        }
        text: qsTr("Empty folder")
        font.pixelSize: Const.FONT_XXXXLARGE
        font.italic: true
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND

        Behavior on opacity {PropertyAnimation{duration: Session.loading ? 100 : 250}}
    }



    ListView{
        id: list

        property bool footerVisible: true

        enabled: !Session.loading
        opacity: {
            if (Session.loading && BModel.count){
                return 0.5
            }
            else if (Session.loading && !BModel.count){
                return 0;
            }
            else{
                return 1.0
            }
        }
        anchors {
            fill: parent
            topMargin: devMargin
        }
        model: BModel
        delegate: BoxPageDelegate{
            onClicked: {
                if (isDir) {
                    Session.listFolder(id)
                    list.footerVisible = true
                }else{
                    if (Settings.multi_selection){
                        if (selected){
                            BModel.setProperty(index, "selected", false)
                        }else{
                            BModel.setProperty(index, "selected", true)
                        }
                    }else{
                        Session.downloadItem(id, name, size)
                    }
                }
            }
            onPressAndHold: {
                if (Session.action !== "normal"){
                }else{
                    if (Session.isSelected() && Settings.multi_selection){
                        menuSelected.open()
                    }else{
                        if (isDir){
                            menuLongPressDir.checkLink = (share_link === undefined)
                            menuLongPressDir.isSynced = sync_state
                            menuLongPressDir.open()
                        }else{
                            menuLongPressFile.checkLink = (share_link === undefined)
                            menuLongPressFile.open()
                        }
                    }
                }
            }
        }
        clip: true
        cacheBuffer: 1000
        section.criteria: ViewSection.FirstCharacter
        section.property: "name"
        footer: Button{
            visible: BModel.count >= Settings.itemsLimit && list.footerVisible
            height: visible ? Const.BUTTON_HEIGHT : 0
            width: parent.width/2
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Load more")
            onClicked: {
                Session.listFolder(Session.currentFolder, BModel.count)
            }
        }
        Behavior on opacity {PropertyAnimation{duration: Session.loading ? 50 : 250}}
    }


    FastScroll{
        listView: list
        visible: BModel.count
    }
}
