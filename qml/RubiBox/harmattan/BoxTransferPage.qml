import QtQuick 1.1
import com.nokia.meego 1.1

import "Component"
import "../scripts/createobject.js" as ObjectCreator
import "../scripts/const.js" as Const
import "FastScroll"
import "Delegates"
import "Buttons"

DevPDA{
    id: root


    function showCancelDialogDialog(){

    }


    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
        ToolIcon{

        }
    }

    devText: qsTr("Transfers")

    Connections{
        target: Box
        onRemoveFromQueueDone: TModel.removeItem(list.currentIndex)
    }


    Menu{
        id: menu

        property bool isUpload: false

        MenuLayout{    

           MenuItem{
                text: menu.isUpload ? qsTr("Cancel upload") : qsTr("Cancel download");
                onClicked: {
                    menu.close();
                    Box.cancelDownload()
                }
            }
        }
    }

    Menu{
        id: menuQueue
        visualParent: root.pageStack

        property string id

        MenuLayout{
            MenuItem{
                text: qsTr("Remove from queue");
                onClicked: {
                    menu.close();
                    Box.removeFromQueue(menuQueue.id)
                }
            }
        }
    }


    Label{
        id: noItems
        opacity: TModel.count ? 0 : 1
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: parent.height/2
        }
        text: qsTr("No transfers")
        font.pixelSize: Const.FONT_XXXXLARGE
        font.italic: true
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND

        Behavior on opacity {PropertyAnimation{duration: 200}}
    }

    ListView{
        id: list
        anchors {
            fill: parent
            topMargin: devMargin
        }
        model: TModel
        delegate: BoxTransferPageDelegate{
            onClicked: {
                if (active){
                    menu.isUpload = !download
                    if (menu.status === DialogStatus.Open) { menu.close() } else { menu.open() }
                }else{
                    menuQueue.id = id
                    if (menuQueue.status === DialogStatus.Open) { menuQueue.close() } else { menuQueue.open() }
                }
            }
        }
        clip: true
        cacheBuffer: 1000
    }

    ScrollDecorator{
        flickableItem: list
    }
}

