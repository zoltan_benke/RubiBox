// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/const.js" as Const
import "../../scripts/createobject.js" as ObjectCreator
import "../Component"


Page{
    id: root

    property alias devText: textBottom.text
    property alias devTextTop: textTop.text
    property alias devShow: header.visible
    property int devMargin: devShow ? header.height : 0
    property bool loadingVisible: true
    property bool doLoading: false

    orientationLock: {
        switch(Settings.orientation){
        case 1:
            return PageOrientation.LockPortrait
        case 2:
            return PageOrientation.LockLandscape
        default:
            return PageOrientation.Automatic
        }
    }

    Rectangle{
        id: header
        z: 1000
        anchors {
            left: parent.left;
            right: parent.right;
            top: parent.top
        }
        width: parent.width
        height: inPortrait ? Const.HEADER_DEFAULT_HEIGHT_PORTRAIT+5 : Const.HEADER_DEFAULT_HEIGHT_LANDSCAPE+5
        clip: true
        color: !Settings.inverted ? "#d3d3d3" : "#262626"
        Rectangle{
            id: headerLine
            anchors.bottom: parent.bottom
            width: parent.width
            clip: true
            height: 5
            color: Settings.activeColor
        }

        Column{
            visible: inPortrait
            anchors{
                left: parent.left
                right: busy.left
                verticalCenter: parent.verticalCenter
                margins: Const.PADDING_LARGE
            }
            spacing: 3
            Label{
                id: textTop
                width: (text === "") ? undefined : parent.width
                elide: Text.ElideLeft
                color: Settings.inverted ? "white" : "black"
                font.pixelSize: Const.FONT_LARGE
            }
            Label{
                id: textBottom
                width: parent.width
                elide: Text.ElideLeft
                color: Settings.inverted ? "white" : "black"
                font.pixelSize: (textTop.text !== "") ? Const.FONT_SMALL : Const.FONT_LARGE
            }
        }


        Label{
            visible: !inPortrait
            anchors{
                left: parent.left
                right: busy.left
                verticalCenter: parent.verticalCenter
                margins: Const.PADDING_LARGE
            }

            elide: Text.ElideLeft
            color: Settings.inverted ? "white" : "black"
            font.pixelSize: Const.FONT_SLARGE
            text: textBottom.text+textTop.text
        }


        BusyIndicator{
            id: busy
            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
                rightMargin: Const.PADDING_LARGE
            }
            running: visible
            visible: Session.loading && loadingVisible || doLoading && loadingVisible
            BusyIndicatorStyle{size: inPortrait ? "medium" : "small"}
        }
    }
}
