// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../../scripts/const.js" as Const
import "../../common"


Item{
    id: root
    width: ListView.view.width

    height: Const.LIST_ITEM_HEIGHT_DEFAULT


    signal clicked
    signal pressAndHold


    opacity: (!isDir && size > Settings.maxUpSize) ? 0.7 : 1
    enabled: opacity !== 0.7


    MoreIndicator {
        id: indicator
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        visible: isDir
    }


    Rectangle{
        id: uploadInfo
        visible: !isDir
        anchors {left: parent.left;}
        height: parent.height
        width: 5
        color: (size > Settings.maxUpSize) ? "red" : "green"
    }


    HLine{
        anchors.bottom: parent.bottom
    }


    Rectangle{
        anchors.fill: parent
        opacity: selected ? 1 : 0
        color: Settings.activeColor

        Behavior on opacity {PropertyAnimation{duration: selected ? 400 : 50}}
    }


    BorderImage {
        id: selectedBackground
        anchors.fill: parent
        opacity: mouseArea.pressed ? 1 : 0
        source: theme.inverted ?
                    'image://theme/meegotouch-list-fullwidth-inverted-background-pressed-vertical-center' :
                    'image://theme/meegotouch-list-fullwidth-background-pressed-vertical-center'
    }


    Image{
        id: thumb
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: Const.PADDING_MEDIUM
        }

        sourceSize.width: !Settings.nativeIcons ? 85 : 65
        sourceSize.height: !Settings.nativeIcons ? 85 : 65
        width: sourceSize.width
        height: sourceSize.height
        smooth: true
        source: Helper.getIcon(Settings.inverted, isDir, 2, Settings.nativeIcons, name)
    }


    Label{
        id: nameLabel
        anchors {
            left: thumb.right
            top: parent.top
            right: isDir ? indicator.left : parent.right
            margins: Const.PADDING_MEDIUM
        }
        opacity: isHidden ? 0.6 : 1
        verticalAlignment: Text.AlignTop
        elide: Text.ElideRight
        font.pixelSize: Const.FONT_LARGE
        text: name
        color: !Settings.inverted ? "black" : "white"
    }



    Label{
        id: sizeLabel
        anchors {
            left: thumb.right
            bottom: parent.bottom
            margins: Const.PADDING_MEDIUM
        }
        text: isDir ? "folder" : Helper.convSize(size)
        font.pixelSize: Const.FONT_SMALL
        color: selected ? !Settings.inverted ? "black" : "white" : Const.COLOR_INVERTED_SECONDARY_FOREGROUND
    }



    MouseArea{
        id: mouseArea
        anchors {
            fill: parent
        }
        onPressed: {
            Helper.playEffect(Helper.ThemeBasicItem)
            list.currentIndex = index
        }
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress);
            root.pressAndHold()
        }
    }
}
