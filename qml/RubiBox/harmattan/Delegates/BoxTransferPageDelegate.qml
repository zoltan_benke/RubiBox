// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../../scripts/const.js" as Const
import "../../common"


Item{
    id: root
    width: ListView.view.width

    height: active ? Const.LIST_ITEM_HEIGHT_DEFAULT + 20 : Const.LIST_ITEM_HEIGHT_DEFAULT + 5

    Behavior on height {PropertyAnimation{duration: 500}}


    signal clicked
    signal pressAndHold


    HLine{
        anchors.bottom: parent.bottom
    }


    Image{
        id: thumb
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: Const.PADDING_MEDIUM
        }
        sourceSize.width: !Settings.nativeIcons ? 85 : 65
        sourceSize.height: !Settings.nativeIcons ? 85 : 65
        width: sourceSize.width
        height: sourceSize.height
        smooth: true
        source: Helper.getIcon(Settings.inverted, false, 2, Settings.nativeIcons, name)
    }

    Label{
        id: nameLabel
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? Const.PADDING_MEDIUM : Const.PADDING_XLARGE
            top: parent.top
            right: parent.right
            margins: Const.PADDING_MEDIUM
        }
        verticalAlignment: Text.AlignTop
        elide: Text.ElideRight
        font.pixelSize: Const.FONT_LARGE
        text: name
        color: !Settings.inverted ? "black" : "white"
    }


    Image{
        id: transferIcon
        anchors{
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? Const.PADDING_MEDIUM : Const.PADDING_XLARGE
            top: nameLabel.bottom
            margins: Const.PADDING_MEDIUM
        }
        smooth: true
        source: download ? "../Images/download.png" : "../Images/upload.png"
        //sourceSize.width: sizeLabel.height
        //sourceSize.height: sizeLabel.height
        //width: sourceSize.width
        //height: sourceSize.width
    }


    Label{
        id: sizeLabel
        anchors {
            left: transferIcon.right
            leftMargin: Const.PADDING_MEDIUM
            right: parent.right
            top: nameLabel.bottom
            margins: Const.PADDING_MEDIUM
        }
        text: qsTr("%1 / %2 | %3").arg(Helper.convSize(received_size)).arg(Helper.convSize(size)).arg(speed)
        font.pixelSize: Const.FONT_SMALL
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
    }

    ProgressBar{
        id: progressBar
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? Const.PADDING_MEDIUM : Const.PADDING_XLARGE
            right: parent.right
            top: sizeLabel.bottom
            margins: Const.PADDING_MEDIUM
        }
        minimumValue: 0
        maximumValue: 100
        value: (received_size*100)/size
        opacity: active ? 1 : 0

        Behavior on opacity {PropertyAnimation{duration: 500}}
    }



    MouseArea{
        id: mouseArea
        anchors {
            fill: parent
        }
        onPressed: {
            Helper.playEffect()
            list.currentIndex = index
        }
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress);
            root.pressAndHold()
        }
    }

}
