// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/createobject.js" as ObjectCreator
import "../../scripts/const.js" as Const

Dialog{
    id: root


    function showRemoveDialog(){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("AccountRemoveDialog.qml"), window.pageStack)
        dialog.itemName = itemName
        dialog.open()
    }

    property string itemName


    content: Item {
        id: deleteName
        height: 50
        width: parent.width
        Label {
            id: deleteText
            font.pixelSize: Const.FONT_LARGE
            font.bold: true
            anchors.centerIn: parent
            color: Settings.activeColor
            text: itemName
        }
    }

    buttons:  ButtonColumn {
        id: buttonsDelete
        spacing: 5
        style: ButtonStyle { }
        anchors.horizontalCenter: parent.horizontalCenter
        exclusive: false
        Button {
            text: qsTr("Set as active")
            onClicked: {
                Helper.playEffect()
                Database.setAsActive(itemName)
                root.accept()
            }
        }
        Button {
            text: qsTr("Delete account")
            onClicked: {
                Helper.playEffect()
                showRemoveDialog()
                root.accept()
            }
        }
        Button {text: qsTr("Cancel"); onClicked: root.close()}
    }

}
