// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../Component"
import "../../scripts/createobject.js" as ObjectCreator
import "../../scripts/const.js" as Const

DevPDA{
    id: root

    function addAccount(username){
        if (Database.addAccount(username, 1)){
            var addAccount = ObjectCreator.createObject(Qt.resolvedUrl("../Auth/AddAccount.qml"), window.pageStack)
            addAccount.accountName = username
            addAccount.open()
            usernameInput.text = "";
            usernameInput.platformCloseSoftwareInputPanel();
            flicker.focus = true;
            return;
        }
        console.debug("UNABLE ADD ACCOUNT")
    }


    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back"
            onClicked: Qt.quit()
        }
    }

    devText: qsTr("Welcome")
    loadingVisible: false

    Flickable{
        id: flicker
        anchors {
            fill: parent
            topMargin: devMargin+Const.MARGIN_XLARGE
            margins: Const.MARGIN_XLARGE
        }
        flickableDirection: Flickable.VerticalFlick
        contentHeight: content.height


        Column{
            id: content
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: 10


            Row{
                spacing: 10
                width: parent.width

                Image{
                    id: imageLogo
                    sourceSize.width: 100
                    sourceSize.height: 100
                    width: sourceSize.width
                    height: sourceSize.height
                    smooth: true
                    source: "../Images/logo.png"
                }

                Column{
                    spacing: 5
                    anchors {
                        top: parent.top
                    }
                    width: parent.width - imageLogo.width - Const.MARGIN_XLARGE

                    Label{
                        font.pixelSize: Const.FONT_XLARGE
                        text: qsTr("RubiBox")
                        font.bold: true
                    }

                    Label{
                        font.pixelSize: Const.FONT_DEFAULT
                        font.italic: true
                        text: qsTr("A fully-featured Box.com client")
                    }
                }
            }

            Label{
                id: label
                width: parent.width
                wrapMode: Text.WordWrap
                text: qsTr("<u>RubiBox</u> needs access to your Box.com account. <u>RubiBox</u> supports multiple accounts. ")+
                      qsTr("Please, enter the display name of the first account for his later identification.")+
                      qsTr(" Later, you can add other accounts.")
                font.pixelSize: Const.FONT_SLARGE
            }

            Row{
                spacing: 5
                width: parent.width

                TextField{
                    id: usernameInput
                    width: parent.width - addBtn.width - Const.MARGIN_XLARGE
                    placeholderText: qsTr("Display name")
                    inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                    Keys.onEnterPressed: {
                        platformCloseSoftwareInputPanel();
                        addAccount(text)
                    }
                    Keys.onReturnPressed: {
                        platformCloseSoftwareInputPanel();
                        addAccount(text)
                    }
                }
                Button{
                    id: addBtn
                    width: height
                    iconSource: Settings.inverted ? "image://theme/icon-m-toolbar-add-white" : "image://theme/icon-m-toolbar-add"
                    onClicked: addAccount(usernameInput.text)
                    enabled: usernameInput.text != ""
                }
            }

            Label{
                id: privacy
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                text: qsTr("<a style='color:%1' href='http://localhost'>Privacy policy</a>").arg(Settings.activeColor)
                font.pixelSize: Const.FONT_SLARGE
                font.bold: true
                onLinkActivated: {
                    Helper.playEffect()
                    root.pageStack.push(Qt.resolvedUrl("../Settings/AboutPage.qml"))
                }
            }
        }
    }
}
