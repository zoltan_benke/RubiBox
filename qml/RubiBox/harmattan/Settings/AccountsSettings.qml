import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../Component"
import "../Delegates"
import "../Buttons"
import "../../scripts/const.js" as Const
import "../../scripts/createobject.js" as ObjectCreator

DevPDA{
    id: root


    function addAccount(username){
        if (Database.addAccount(username, 0)){
            var addAccount = ObjectCreator.createObject(Qt.resolvedUrl("../Auth/AddAccount.qml"), root.pageStack)
            addAccount.accountName = username
            addAccount.open()
            usernameInput.text = "";
            usernameInput.platformCloseSoftwareInputPanel();
            flick.focus = true;
            return;
        }
        console.debug("UNABLE ADD ACCOUNT")
    }


    function setActiveAccount(){
        Database.setAsActive(accountsModel.get(listView.currentIndex).name)
    }


    function showAccountsDialog(name){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("../Dialogs/AccountsDialog.qml"), root.pageStack)
        dialog.itemName = name
        dialog.open()
    }


    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: if (menu.status === DialogStatus.Open) {menu.close()} else {menu.open()}
        }
    }


    Menu{
        id: menu
        MenuLayout{
            MenuItem{text: qsTr("Delete all accounts");
                onClicked: {
                    if (Database.clearAllAccounts()){
                        root.pageStack.replace(Qt.resolvedUrl("../FirstStart/BoxFirstPage.qml"))
                    }else{
                        infoBanner.parent = root.pageStack
                        infoBanner.text = qsTr("Unable delete accounts")
                        infoBanner.show()
                    }
                }
            }
        }
    }


    devText: qsTr("Settings - Accounts")
    loadingVisible: false
    onStatusChanged: {
        if (status === PageStatus.Activating && !accountsModel.count){
            var list = Database.accounts
            for (var index in list){
                accountsModel.append({"name": list[index]})
            }
        }
    }


    Connections{
        target: Database
        onAccountsCountChanged: {
            accountsModel.clear()
            var list = Database.accounts
            for (var index in list){
                accountsModel.append({"name": list[index]})
            }
        }
        onActiveAccountChanged: {
            accountsModel.clear()
            var list = Database.accounts
            for (var index in list){
                accountsModel.append({"name": list[index]})
            }
        }
    }


    Flickable{
        id: flick
        anchors {
            fill: parent
            topMargin: devMargin
            margins: Const.MARGIN_XLARGE
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            anchors{
                left: parent.left
                right: parent.right
            }

            spacing: 20

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Add account")
            }

            Row{
                spacing: 5
                width: parent.width

                TextField{
                    id: usernameInput
                    width: parent.width - addBtn.width
                    placeholderText: qsTr("Display name")
                    inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                    Keys.onEnterPressed: {
                        platformCloseSoftwareInputPanel();
                        addAccount(text)
                    }
                    Keys.onReturnPressed: {
                        platformCloseSoftwareInputPanel();
                        addAccount(text)
                    }
                }
                Button{
                    id: addBtn
                    width: height
                    iconSource: Settings.inverted ? "image://theme/icon-m-toolbar-add-white" : "image://theme/icon-m-toolbar-add"
                    onClicked: addAccount(usernameInput.text)
                    enabled: usernameInput.text != ""
                }
            }


            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Active account")
            }

            Row{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                Label{
                    anchors.verticalCenter: infoBtn.verticalCenter
                    font.bold: true
                    text: Database.active_account
                    width: parent.width - infoBtn.width
                    font.pixelSize: Const.FONT_LARGE
                }
                Image{
                    id: infoBtn
                    source: !Settings.inverted ? "../Images/info-inverted.png"
                                            : "../Images/info.png"
                    sourceSize.width: Const.SIZE_ICON_LARGE
                    sourceSize.height: Const.SIZE_ICON_LARGE
                    cache: false

                    MouseArea{
                        anchors.fill: parent
                        onPressed: Helper.playEffect(Helper.ThemeBasicButton)
                        onClicked: {
                            root.pageStack.push(Qt.resolvedUrl("../BoxAccountInfoPage.qml"))
                        }
                    }
                }
            }

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Others accounts")
            }


            ListView{
                id: listView
                visible: (Database.accountsCount > 1)
                width: parent.width
                height: Const.LIST_ITEM_HEIGHT_SMALL * (Database.accountsCount-1)
                model: ListModel{
                    id: accountsModel
                }
                delegate: AccountsSettingsDelegate{
                    id: accountsDelegate
                    onClicked: {
                        showAccountsDialog(name)
                    }
                }
                interactive: false
                //clip: true
            }

            Label{
                id: noAccountsLabel
                visible: !(Database.accountsCount > 1)
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("No others accounts")
                color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
            }
        }
    }




}
