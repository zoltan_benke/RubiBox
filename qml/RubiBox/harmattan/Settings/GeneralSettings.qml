import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../Component"
import "../Delegates"
import "../Buttons"
import "../../scripts/const.js" as Const
import "../../scripts/createobject.js" as ObjectCreator

DevPDA{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }



    devText: qsTr("Settings - General")
    loadingVisible: false
    onStatusChanged: {
        if (status === PageStatus.Activating){
        }
    }


    Flickable{
        id: flicker
        anchors {
            fill: parent
            topMargin: devMargin
            margins: Const.MARGIN_XLARGE
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            spacing: 20
            width: parent.width

            Separator{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Items limit")
            }

            Column{
                width: parent.width

                Label{
                    anchors.horizontalCenter: parent.horizontalCenter
                    maximumLineCount: 2
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("The number of folder items (100-1000)")
                    font{
                        pixelSize: Const.FONT_SMALL
                    }
                }

                Row{
                    spacing: 5
                    width: parent.width

                    TextField{
                        id: maxItems
                        validator: IntValidator{
                            bottom: 100
                            top: 1000
                        }
                        width: parent.width - saveBtn.width
                        text: Settings.itemsLimit
                        inputMethodHints: Qt.ImhDigitsOnly | Qt.ImhNoPredictiveText | Qt.ImhPreferNumbers
                        Keys.onEnterPressed: {
                            if (parseInt(text) >= 100 && parseInt(text) <= 1000 && Settings.itemsLimit !== parseInt(text)){
                                closeSoftwareInputPanel()
                                Settings.itemsLimit = text
                            }
                        }
                        Keys.onReturnPressed: {
                            if (parseInt(text) >= 100 && parseInt(text) <= 1000 && Settings.itemsLimit !== parseInt(text)){
                                closeSoftwareInputPanel()
                                Settings.itemsLimit = text
                            }
                        }
                    }


                    Button{
                        id: saveBtn
                        enabled: (parseInt(maxItems.text) != Settings.itemsLimit) && parseInt(maxItems.text) >= 100 && parseInt(maxItems.text) <= 1000
                        width: height
                        iconSource: !Settings.inverted ? "../Images/accept_black.png" : "../Images/accept_white.png"
                        onClicked: {
                            maxItems.closeSoftwareInputPanel()
                            Settings.itemsLimit = maxItems.text
                        }
                    }
                }
            }

            Separator{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Screen orientation")
            }


            ButtonRow{
                id: btnRow
                anchors.horizontalCenter: parent.horizontalCenter
                checkedButton: {
                    switch(Settings.orientation){
                    case 1:
                        return portraitBtn
                    case 2:
                        return landscapeBtn
                    default:
                        return autoBtn
                    }
                }
                Button{
                    id: autoBtn
                    text: qsTr("Auto")
                    onClicked: Settings.orientation = 0
                }
                Button{
                    id: portraitBtn
                    text: qsTr("Portrait")
                    onClicked: Settings.orientation = 1
                }
                Button{
                    id: landscapeBtn
                    text: qsTr("Landscape")
                    onClicked: Settings.orientation = 2
                }
            }
            /*
            Separator{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Divided sections %1").arg("<font size=\"2\">(box files list)")
            }


            Row{
                width: parent.width

                Label{
                    id: divideSectionsLabel
                    width: parent.width - divideSectionsSwitch.width
                    text: qsTr("Divide sections")
                }

                Switch{
                    id: divideSectionsSwitch
                    anchors.verticalCenter: divideSectionsLabel.verticalCenter
                    checked: Settings.sectionSeparator
                    onCheckedChanged: Settings.sectionSeparator = checked
                }
            }
            */
        }
    }
}
