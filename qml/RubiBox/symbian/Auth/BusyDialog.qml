import QtQuick 1.1
import com.nokia.symbian 1.1

Item {
    id: dialog

    property alias message : message.text

    width: message.width + 70
    height: 60
    opacity: dialog.visible ? 1 : 0

    Behavior on opacity { PropertyAnimation { properties: "opacity"; duration: 500 } }

    BusyIndicator {
        id: busyIndicator

        anchors { left: dialog.left; leftMargin: 10; verticalCenter: dialog.verticalCenter }
        running: dialog.visible
    }

    Label {
        id: message
        platformInverted: Settings.inverted
        anchors { left: busyIndicator.right; leftMargin: 10; verticalCenter: dialog.verticalCenter }
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Loading...")
    }
}
