import QtQuick 1.1
import com.nokia.symbian 1.1

import "Component"
import "../scripts/createobject.js" as ObjectCreator
import "Delegates"
import "Buttons"

DevPDA{
    id: root


    function showCancelDialogDialog(){

    }


    tools: ToolBarLayout{
        ToolIcon{
            iconSource: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }

    devText: qsTr("Transfers")

    Connections{
        target: Box
        onRemoveFromQueueDone: TModel.removeItem(list.currentIndex)
    }


    Menu{
        id: menu

        property bool isUpload: false
        platformInverted: Settings.inverted

        MenuLayout{    

           MenuItem{
                platformInverted: Settings.inverted
                text: menu.isUpload ? qsTr("Cancel upload") : qsTr("Cancel download");
                onClicked: {
                    menu.close();
                    Box.cancelDownload()
                }
            }
        }
    }

    Menu{
        id: menuQueue

        property string id
        platformInverted: Settings.inverted

        MenuLayout{
            MenuItem{
                platformInverted: Settings.inverted
                text: qsTr("Remove from queue");
                onClicked: {
                    menu.close();
                    Box.removeFromQueue(menuQueue.id)
                }
            }
        }
    }


    Label{
        id: noItems
        opacity: TModel.count ? 0 : 1
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: parent.height/2
        }
        text: qsTr("No transfers")
        font.pixelSize: 40
        font.italic: true
        color: platformStyle.colorNormalMid

        Behavior on opacity {PropertyAnimation{duration: 200}}
    }

    ListView{
        id: list
        anchors {
            fill: parent
            topMargin: devMargin
        }
        model: TModel
        delegate: BoxTransferPageDelegate{
            onClicked: {
                if (active){
                    menu.isUpload = !download
                    if (menu.status === DialogStatus.Open) { menu.close() } else { menu.open() }
                }else{
                    menuQueue.id = id
                    if (menuQueue.status === DialogStatus.Open) { menuQueue.close() } else { menuQueue.open() }
                }
            }
        }
        clip: true
        cacheBuffer: 1000
    }

    ScrollDecorator{
        flickableItem: list
    }
}

