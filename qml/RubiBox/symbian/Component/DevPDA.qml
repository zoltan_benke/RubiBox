// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

import "../Component"


Page{
    id: root

    property alias devText: textBottom.text
    property alias devTextTop: textTop.text
    property alias devShow: header.visible
    property int devMargin: devShow ? header.height : 0
    property bool loadingVisible: true
    property bool doLoading: false

    orientationLock: {
        switch(Settings.orientation){
        case 1:
            return PageOrientation.LockPortrait
        case 2:
            return PageOrientation.LockLandscape
        default:
            return PageOrientation.Automatic
        }
    }

    Rectangle{
        id: header
        z: 1000
        anchors {
            left: parent.left;
            right: parent.right;
            top: parent.top
        }
        width: parent.width
        height: inPortrait ? 60 : 50
        clip: true
        color: Settings.inverted ? "#d3d3d3" : "#262626"
        Rectangle{
            id: headerLine
            anchors.bottom: parent.bottom
            width: parent.width
            clip: true
            height: 5
            color: Settings.activeColor
        }

        Column{
            visible: inPortrait
            anchors{
                left: parent.left
                right: busy.left
                verticalCenter: parent.verticalCenter
                margins: platformStyle.paddingMedium
            }
            spacing: 0
            Label{
                id: textTop
                platformInverted: Settings.inverted
                width: (text === "") ? undefined : parent.width
                elide: Text.ElideLeft
                font.pixelSize: platformStyle.fontSizeLarge
            }
            Label{
                id: textBottom
                platformInverted: Settings.inverted
                width: parent.width
                elide: Text.ElideLeft
                font.pixelSize: (textTop.text !== "") ? platformStyle.fontSizeSmall : platformStyle.fontSizeLarge
            }
        }


        Label{
            visible: !inPortrait
            platformInverted: Settings.inverted
            anchors{
                left: parent.left
                right: busy.left
                verticalCenter: parent.verticalCenter
                margins: platformStyle.paddingMedium
            }

            elide: Text.ElideLeft
            font.pixelSize: platformStyle.fontSizeLarge
            text: textBottom.text+textTop.text
        }


        BusyIndicator{
            id: busy
            platformInverted: Settings.inverted
            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
                rightMargin: platformStyle.paddingMedium
            }
            running: visible
            visible: Session.loading && loadingVisible || doLoading && loadingVisible
        }
    }
}
