import QtQuick 1.1
import com.nokia.symbian 1.1

Rectangle {
    id: root

    property string acceptButtonText
    property string rejectButtonText
    property alias content: contentField.children
    property bool destroyWhenClosed: true
    property bool hideHeaderWhenInputContextIsVisible: true
    property bool showToolBarWhenClosed: true
    property string status: "closed"

    signal opened
    signal closed
    signal accepted
    signal rejected

    function open() {
        root.status = "open";
        root.opened();
    }

    function close() {
        root.status = "closed";
        root.closed();

        if (root.destroyWhenClosed) {
            root.destroy(600);
        }
    }

    function accept() {
        root.accepted();
        root.close();
    }

    function reject() {
        root.rejected();
        root.close();
    }

    z: 500000000
    y: root.status == "closed" ? 640 : 0
    width: parent.width
    height: window.pageStack.height
    color: Settings.inverted ? platformStyle.colorBackgroundInverted : platformStyle.colorBackground
    visible: y < 640
    onVisibleChanged: (visible) ? window.showToolBar = false : window.showToolBar = true

    Behavior on y { NumberAnimation { easing.type: Easing.OutQuint; duration: 300 } }


    CornerImage {
        z: 1000
    }

    ListHeading {
        id: header

        z: 999
        anchors { left: parent.left; right: parent.right; top: parent.top }
        height: 60
        platformInverted: Settings.inverted

        Button {
            id: rejectButton
            platformInverted: Settings.inverted
            width: window.inPortrait ? 150 : 180
            anchors { left: parent.left; leftMargin: platformStyle.paddingMedium; verticalCenter: parent.verticalCenter }
            text: rejectButtonText
            visible: (header.height == 60) && (rejectButtonText != "")
            onClicked: root.reject()
        }

        Button {
            id: acceptButton
            platformInverted: Settings.inverted
            width: window.inPortrait ? 150 : 180
            anchors { right: parent.right; rightMargin: platformStyle.paddingMedium; verticalCenter: parent.verticalCenter }
            text: acceptButtonText
            visible: (header.height == 60) && (acceptButtonText != "")
            onClicked: root.accept()
        }

        Rectangle {
            height: 1
            anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
            color: Settings.activeColor
            opacity: 0.5
        }
    }

    Item {
        id: contentField

        anchors { fill: parent; topMargin: header.height }
        clip: true
    }

    MouseArea {
        z: -1
        anchors.fill: parent
    }

    states: State {
        name: "textInputMode"
        when: (inputContext.visible) && (hideHeaderWhenInputContextIsVisible)
        PropertyChanges { target: header; height: 0 }
    }

    transitions: Transition {
        NumberAnimation { target: header; properties: "height"; duration: 200 }
    }
}
