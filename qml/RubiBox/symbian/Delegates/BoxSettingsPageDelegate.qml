// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../common"
import "../Component"

Item{
    id: root
    width: ListView.view.width

    height: 60


    signal clicked


    HLine{
        visible: true
        anchors.bottom: parent.bottom
    }


    Image {
        id: indicator
        anchors{
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        source: privateStyle.imagePath("qtg_graf_drill_down_indicator", Settings.inverted)
        sourceSize.width: platformStyle.graphicSizeSmall
        sourceSize.height: platformStyle.graphicSizeSmall
    }


    BorderImage {
        id: selectedBackground
        anchors.fill: parent
        visible: mouseArea.pressed
        source: Settings.inverted ?
                    '../Images/meegotouch_list-fullwidth_inverted_background_pressed_vertical_center.png' :
                    '../Images/meegotouch_list-fullwidth_background_pressed_vertical_center.png'
        width: parent.width
        height: parent.height
    }


    Label{
        id: nameLabel
        platformInverted: Settings.inverted
        anchors {
            fill: parent
            leftMargin: 10
            rightMargin: 60
        }
        verticalAlignment: Text.AlignVCenter
        text: name
    }


    MouseArea{
        id: mouseArea
        anchors.fill: parent
        onClicked: {
            root.clicked()
        }
        onPressed: {
            list.currentIndex = index
            Helper.playEffect(Helper.ThemeBasicItem)
        }
    }
}
