// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

import "../../scripts/createobject.js" as ObjectCreator

SelectionDialog {
    id: root

    property string shareLink

    function shareFunctions(id){
        switch (id){
        case 0:
            Helper.copy(shareLink)
            break;
        case 1:
            Qt.openUrlExternally("sms:?body="+shareLink)
            break;
        case 2:
            Qt.openUrlExternally("mailto:?subject=&body="+shareLink)
            break;
        }
    }


    titleText: qsTr("<font color=\"%1\">Share</font>").arg(Settings.activeColor)
    platformInverted: Settings.inverted
    onSelectedIndexChanged: shareFunctions(selectedIndex)
    model: ListModel {
        id: list
        ListElement { name: "" }
        ListElement { name: "" }
        ListElement { name: "" }

        Component.onCompleted: {
            list.get(0).name = qsTr("Copy")
            list.get(1).name = qsTr("Sms")
            list.get(2).name = qsTr("Email")
        }
    }
    delegate: MenuItem{
        platformInverted: Settings.inverted
        text: name
        onClicked: {
            Helper.playEffect()
            root.selectedIndex = index
            root.accept()
        }
    }
}
