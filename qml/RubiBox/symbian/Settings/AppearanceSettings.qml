// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../common"
import "../Component"
import "../Delegates"
import "../Buttons"

DevPDA{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            iconSource: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }





    devText: qsTr("Settings - Appearance")
    loadingVisible: false


    Flickable{
        id: flicker
        anchors {
            fill: parent
            topMargin: devMargin
            margins: platformStyle.paddingLarge
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            spacing: 20
            width: parent.width

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Icons")
            }

            ButtonRow{
                width: parent.width
                checkedButton: Settings.nativeIcons ? nativeBtn : customBtn
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    id: customBtn
                    platformInverted: Settings.inverted
                    text: qsTr("Custom")
                    onClicked: Settings.nativeIcons = false
                }
                Button{
                    id: nativeBtn
                    platformInverted: Settings.inverted
                    text: qsTr("Native")
                    onClicked: Settings.nativeIcons = true
                }
            }


            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Theme style")
            }

            ButtonRow{
                width: parent.width
                checkedButton: Settings.inverted ? lightBtn : darkBtn
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    id: lightBtn
                    platformInverted: Settings.inverted
                    text: qsTr("Light")
                    onClicked: Settings.inverted = true
                }
                Button{
                    id: darkBtn
                    platformInverted: Settings.inverted
                    text: qsTr("Dark")
                    onClicked: Settings.inverted = false
                }
            }

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Theme color")
            }

            Flow{
                id: flow
                spacing: 5
                width: parent.width
                Repeater {
                    model: ListModel {
                        ListElement { name: 'color2'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#65b905" }
                        ListElement { name: 'color3'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#418c11" }
                        ListElement { name: 'color4'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#37790c" }
                        ListElement { name: 'color5'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#346905" }
                        ListElement { name: 'color6'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#0da9cd" }
                        ListElement { name: 'color7'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#0480ca" }
                        ListElement { name: 'color8'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#056abe" }
                        ListElement { name: 'color9'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#2054b1" }
                        ListElement { name: 'color10'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#6604bd" }
                        ListElement { name: 'color11'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#8a11bc" }
                        ListElement { name: 'color12'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#cd0ebc" }
                        ListElement { name: 'color13'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#e905a3" }
                        ListElement { name: 'color14'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#ef5704" }
                        ListElement { name: 'color15'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#ea680f" }
                        ListElement { name: 'color16'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#f7741d" }
                        ListElement { name: 'color17'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#ff8805" }
                        ListElement { name: 'color18'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#ed970b" }
                        ListElement { name: 'color19'; prefix: '-meegotouch-button-accent-background.png'; colorString: "#f2b315" }
                    }
                    delegate: MouseArea {
                        width: 42
                        height: 42
                        Image {
                            id: icon
                            anchors.centerIn: parent
                            source: '../Images/Colors/'.concat(model.name).concat(model.prefix)
                            scale: Settings.activeColor === model.colorString ? 1.30 : 1.0
                            smooth: true
                            width: 39
                            height: 39
                            Behavior on scale { NumberAnimation { easing.type: Easing.OutBack } }
                        }
                        onPressed: Helper.playEffect(Helper.ThemeBlankSelection)
                        onClicked: {
                            Settings.activeColor = model.colorString
                            Settings.activeColorString = model.name.concat("-")
                            Settings.activeColorScheme = model.index+2
                        }
                    }
                }
            }

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Horizontal line")
            }

            Row{
                width: parent.width

                Label{
                    id: labelHLine
                    platformInverted: Settings.inverted
                    width: parent.width - switchHLine.width
                    text: qsTr("Colorize horizontal line")
                }

                Switch{
                    id: switchHLine
                    platformInverted: Settings.inverted
                    anchors.verticalCenter: labelHLine.verticalCenter
                    checked: Settings.colorizeHLine
                    onCheckedChanged: Settings.colorizeHLine = checked
                }
            }

            Label{
                platformInverted: Settings.inverted
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Preview:")
            }

            HLine{
                visible: true
            }
        }
    }
}
