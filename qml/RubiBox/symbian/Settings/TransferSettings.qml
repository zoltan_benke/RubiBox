import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../Component"
import "../Delegates"
import "../Buttons"
import "../../scripts/createobject.js" as ObjectCreator

DevPDA{
    id: root


    function showFolderSelect(){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("../Dialogs/FolderSelect.qml"), root.pageStack)
        dialog.open()
    }


    tools: ToolBarLayout{
        ToolIcon{
            iconSource: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }



    devText: qsTr("Settings - Transfers")
    loadingVisible: false


    Flickable{
        id: flicker
        anchors {
            fill: parent
            topMargin: devMargin
            margins: platformStyle.paddingLarge
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            spacing: 20
            width: parent.width

            Separator{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Download folder")
            }

            Row{
                spacing: 5
                width: parent.width

                TextField{
                    id: folderInput
                    platformInverted: Settings.inverted
                    readOnly: true
                    width: parent.width - editBtn.width
                    text: Settings.downloadFolder
                }
                Button{
                    id: editBtn
                    platformInverted: Settings.inverted
                    width: height
                    iconSource: Settings.inverted ? "../Images/edit.png" : "../Images/edit_white.png"
                    onClicked: showFolderSelect()
                }
            }

        }
    }
}
