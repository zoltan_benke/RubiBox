import QtQuick 1.1

Item {
    id: splashScreen




    Image {
        anchors.fill: parent
        source: "Images/background.png"
    }

    Column {
        height: childrenRect.height
        anchors.centerIn: parent
        spacing: 5

        Image{
            source: "Images/logo_splash.png"
            height: sourceSize.height
            width: sourceSize.width
            sourceSize.width: 120
            sourceSize.height: 120
            smooth: true
            clip: true
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }


    Behavior on opacity { NumberAnimation { duration: 350 } }
}
