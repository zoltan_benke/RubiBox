// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

import "../Buttons"
import "../../scripts/utils.js" as Utils

ToolBarLayout{
    id: root


    ToolIcon{
        id: backBtn
        iconSource: Session.isRoot ? platformInverted ? "../Images/close.png" : "../Images/close_white.png" : platformInverted ? "../Images/up.png" : "../Images/up_white.png"
        onClicked: {
            if (Session.isRoot)
                Qt.quit()
            else{
                Session.listFolder(Session.parentFolder)
            }
        }
        onPressAndHold: {
            if (!Session.isRoot){
                Session.listFolder()
            }
        }
    }
    ToolButton{
        anchors{
            right: cancelBtn.left
            rightMargin: platformStyle.paddingMedium
            left: backBtn.right
            leftMargin: platformStyle.paddingMedium
            verticalCenter: parent.verticalCenter
        }
        flat: false
        onClicked: {
            if (Utils.CM_ITEM_CONFLICT && (Utils.CM_FOLDER_ID !== Session.currentFolder)){
                window.showConflictdialog()
                return;
            }
            if ((Utils.CM_FOLDER_ID !== Session.currentFolder) && !Session.loading){
                Session.moveItem()
            }else{
                infoBanner.parent = window.pageStack
                infoBanner.text = qsTr("Can't move to same folder")
                infoBanner.open()
            }
        }
        text: qsTr("%1Move here").arg((Utils.CM_FOLDER_ID !== Session.currentFolder) ? "<font color=\"green\">" : "<font color=\"red\">")
    }
    ToolButton{
        id: cancelBtn
        flat: false
        anchors{
            right: parent.right
            verticalCenter: parent.verticalCenter
            rightMargin: platformStyle.paddingMedium
        }
        text: qsTr("Cancel")
        onClicked: {
            if (!Session.loading){
                Session.action = "normal"
            }
        }
    }

}
