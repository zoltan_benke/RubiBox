// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "Component"
import "FirstStart"
import "../scripts/utils.js" as Utils
import "../scripts/createobject.js" as ObjectCreator

PageStackWindow{
    id: window


    function checkConflict(name){
        Utils.CM_ITEM_CONFLICT = false;
        for (var i = 0; i < BModel.count; i++){
            if (name == BModel.get(i).name){
                Utils.CM_ITEM_CONFLICT = true
                console.log("ITEM CONFLICT: ".concat(Utils.CM_ITEM_CONFLICT))
                return;
            }
        }
        console.log("ITEM CONFLICT: ".concat(Utils.CM_ITEM_CONFLICT))
    }


    function showConflictdialog(){
        Helper.playEffect(Helper.ThemePopupOpen)
        function okClicked(name){
            if (Session.action === "copy"){
                Session.copyItem(name)
            }
            else if (Session.action === "move"){
                Session.moveItem(name)
            }
        }
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/SimpleInput.qml"), pageStack)
        dialog.okClicked.connect(okClicked)
        dialog.placeHolder = Utils.CM_ITEM_NAME
        dialog.text = Utils.CM_ITEM_NAME
        dialog.titleText = qsTr("Conflict !")
        dialog.open()
    }

    function getCurrentItem(){
        return BModel.get(Session.currentIndex);
    }


    function _selectAllFiles(){
        for (var i = 0; i < BModel.count; ++i){
            BModel.setProperty(i, "selected", true)
        }
    }


    function _deselectAllFiles(){
        for (var i = 0; i < BModel.count; ++i){
            BModel.setProperty(i, "selected", false)
        }
    }


    function addToTransferQueue(id, name, size){
        Utils.addToQueue(id, name, size)
    }


    function showLinkShareDialog(shareLink){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/LinkShareDialog.qml"), window.pageStack)
        dialog.shareLink = shareLink
        dialog.open()
    }


    showStatusBar: inPortrait
    showToolBar: true
    initialPage: Database.authorized ? Qt.resolvedUrl("BoxPage.qml") : Qt.resolvedUrl("FirstStart/BoxFirstPage.qml")
    platformInverted: Settings.inverted
    platformSoftwareInputPanelEnabled: true


    property bool isItemSelected: false



    StatusBar{
        id: statusBar
        visible: inPortrait
        anchors {top: parent.top}
        Label{
            anchors {left: parent.left; verticalCenter: parent.verticalCenter; leftMargin: platformStyle.paddingSmall}
            font.pixelSize: 18
            text: qsTr("RubiBox")
        }
    }


    CornerImage{
        parent: pageStack
    }


    Image{
        z: -1
        source: Qt.resolvedUrl("Images/background.png")
        anchors {fill: parent; topMargin: inPortrait ? 60 : 50}
        visible: !Settings.inverted
        parent: window.pageStack
        fillMode: Image.Stretch
    }


    InfoBanner{
        id: infoBanner
        z: 1000
        iconSource: "Images/bannerImage.png"
    }


    Connections{
        target: Settings
        onInvertedChanged: window.platformInverted = value
        onMultiSelectionChanged: {
            infoBanner.parent = pageStack
            if (value){
                infoBanner.text = qsTr("Multiselection enabled");
            }else{
                _deselectAllFiles()
                infoBanner.text = qsTr("Multiselection disabled");
            }
            infoBanner.open()
        }
        onItemsLimitChanged: {
            infoBanner.parent = pageStack
            infoBanner.text = qsTr("Items limit saved");
            infoBanner.open()
        }
    }


    Connections{
        target: Box
        onCopyActionDone: Session.action = "normal"
        onMoveActionDone: {
            Session.action = "normal"
        }

        onAllTransfersDone: {
            infoBanner.parent = pageStack
            infoBanner.text = qsTr("All transfers finished");
            infoBanner.open()
        }
        onShareLinkDone: {
            getCurrentItem().share_link = shareLink
            showLinkShareDialog(shareLink)
        }
    }


    Connections{
        target: Database
        onActiveAccountChanged: Session.listFolder()
    }


    Component.onCompleted: {
        if (Database.authorized)
            Session.renewToken()
        console.debug(Settings.activeColorString)
    }

}
