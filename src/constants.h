#ifndef CONSTANTS_H
#define CONSTANTS_H

//const char* const API_KEY = "";
const char* const CLIENT_ID = "";
const char* const CLIENT_SECRET = "";

/** Settings groups **/
const char* const APPEARANCE = "APPEARANCE";
const char* const OPTIONS = "OPTIONS";
const char* const BASE = "BASE";


#endif // CONSTANTS_H
