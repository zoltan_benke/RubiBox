#include "download.h"

Download::Download(QObject *parent) :
    QObject(parent)
{
    isActive = false;
}

Download::Download(QNetworkAccessManager *manager, QObject *parent) :
    QObject(parent), m_nam(manager)
{
    isActive = false;
}

Download::Download(TModelItem *item, QObject *parent) :
    QObject(parent), m_item(item)
{
    isActive = false;
}

Download::Download(TModelItem *item, QNetworkAccessManager *manager, QObject *parent) :
    QObject(parent), m_nam(manager), m_item(item)
{
    isActive = false;
}



Download::~Download()
{
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO << "DESTRUCTOR";
#endif
}

void Download::setNetworkAccessManager(QNetworkAccessManager *manager)
{
    m_nam = manager;
}


void Download::setItem(TModelItem *item)
{
    m_item = item;
}



void Download::download()
{
    isActive = true;
    prepareDownload();
}



void Download::prepareDownload()
{
    m_item->setActive(true);
    QUrl url(QString("https://api.box.com/2.0/files/%1/content").arg(m_item->id()));

    QNetworkRequest req(url);
    setHeader(req);
    connect(m_nam, SIGNAL(finished(QNetworkReply*)),
            SLOT(prepareFinished(QNetworkReply*)));
    m_nam->get(req);
}



void Download::prepareFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError){
        QUrl location = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        if (!location.isEmpty()){
            qDebug() << "LOCATION: " << location.toString();
            doDownload(location);
        }
    }else{
        int errorCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute ).toInt();
#ifdef QT_DEBUG
        qDebug() << "ERROR CODE:" << errorCode;
        qDebug() << "ERROR RESPONSE:" << reply->errorString();
        qDebug() << "ERROR RESPONSE:" << reply->readAll();
        qDebug() << "Download failed";
#endif
    }
}

void Download::doDownload(const QUrl url)
{
    disconnect(m_nam, 0, 0, 0);

    QNetworkRequest req(url);

    QDir dir("/home/user/MyDocs/RubiBox/");
    if (!dir.exists()){
        dir.mkpath(dir.path());
    }

#ifdef Q_OS_HARMATTAN
    output.setFileName(QString("/home/user/MyDocs/RubiBox/%1").arg(m_item->name()));
#else
    output.setFileName(QString("E:/RubiBox/%1").arg(m_item->name()));
#endif
    if (!output.open(QIODevice::WriteOnly)) {
#ifdef QT_DEBUG
        qDebug() << "Problem opening file " << output.fileName() << " for download " << output.errorString();
#endif
        emit transferDone();
        return; // skip this download
    }
    reply = m_nam->get(req);
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
            SLOT(progress(qint64,qint64)));\
    connect(reply, SIGNAL(readyRead()),
            SLOT(ready()));
    connect(reply, SIGNAL(finished()),
            SLOT(finished()));
    downloadTime.start();
}


void Download::progress(qint64 bytesReceived, qint64 bytesTotal)
{
    Q_UNUSED(bytesTotal)
    double speed = bytesReceived * 1000.0 / downloadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "b/s";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kb/s";
    } else {
        speed /= 1024*1024;
        unit = "Mb/s";
    }
    //emit received_size(bytesReceived);
    //emit speed(QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
    m_item->setReceivedSize(bytesReceived);
    //m_item->setSpeed(QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
}


void Download::ready()
{
    if (output.isOpen())
        output.write(reply->readAll());
}


void Download::finished()
{
    output.close();
    if (reply->error() == QNetworkReply::NoError){
#ifdef QT_DEBUG
        qDebug() << "File downloaded: " << m_item->name();
#endif
        isActive = false;
        m_item->setActive(false);
        emit transferDone();
    }
    else{
        int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
#ifdef QT_DEBUG
        qDebug() << "ERROR CODE:" << errorCode;
        qDebug() << "ERROR RESPONSE:" << reply->errorString();
        qDebug() << "ERROR RESPONSE:" << reply->readAll();
        qDebug() << "Download failed";
#endif
        isActive = false;
        m_item->setActive(false);
        emit transferDone();
    }
}
