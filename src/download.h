#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include <QObject>
#include <QtNetwork>
#include "constants.h"
#include "tmodelitem.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif



class Download : public QObject
{
    Q_OBJECT
public:
    Download(QObject *parent = 0);
    Download(QNetworkAccessManager *manager = 0, QObject *parent = 0);
    Download(TModelItem *item = 0, QObject *parent = 0);
    Download(TModelItem *item = 0, QNetworkAccessManager *manager = 0, QObject *parent = 0);
    virtual ~Download();

    bool isActive;
    
signals:
    void transferDone(const int index = 0);
    void speed(const QString speed);
    void received_size(const qint64 received_size);


public slots:
    void setNetworkAccessManager(QNetworkAccessManager *manager);
    void setItem(TModelItem *item);
    inline void setToken(const QByteArray token){
        m_token = token;
    }


    void download();



private slots:
    void prepareDownload();
    inline void setHeader(QNetworkRequest &request){
        QByteArray header = "BoxAuth ";
        header += "api_key="+(QByteArray)API_KEY;
        header += "&auth_token="+m_token;
        request.setRawHeader("Authorization", header);
    }

    void prepareFinished(QNetworkReply *reply);
    void doDownload(const QUrl url);

    void progress(qint64 bytesReceived, qint64 bytesTotal);
    void ready();
    void finished();


private:
    QNetworkAccessManager *m_nam;
    QNetworkReply *reply;
    TModelItem*m_item;
    QTime downloadTime;
    QByteArray m_token;
    QFile output;
    
};

#endif // DOWNLOAD_H
