#ifndef FILEBROWSER_H
#define FILEBROWSER_H

#include <QObject>
#include <QAbstractListModel>
#include <QtDeclarative>

#include <QFileInfo>
#include <QDir>


#include <QDebug>

QT_BEGIN_HEADER

QT_MODULE(Declarative)

class QDeclarativeContext;
class QModelIndex;

class FBrowserPrivate;

class FBrowser : public QAbstractListModel, public QDeclarativeParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QDeclarativeParserStatus)
    Q_ENUMS(SortField)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QUrl folder READ folder WRITE setFolder NOTIFY folderChanged)
    Q_PROPERTY(QUrl parentFolder READ parentFolder NOTIFY folderChanged)
    Q_PROPERTY(QStringList nameFilters READ nameFilters WRITE setNameFilters)
    Q_PROPERTY(SortField sortField READ sortField WRITE setSortField)
    Q_PROPERTY(bool sortReversed READ sortReversed WRITE setSortReversed)
    Q_PROPERTY(bool showDirs READ showDirs WRITE setShowDirs)
    Q_PROPERTY(bool showHidden READ showHidden WRITE setShowHidden)
    Q_PROPERTY(bool showOnlyDirs READ showOnlyDirs WRITE setShowOnlyDirs)
public:
    FBrowser(QObject *parent = 0);
    virtual ~FBrowser();

    enum{
        NAME = Qt::UserRole+1,
        PATH = Qt::UserRole+2,
        SIZE = Qt::UserRole+3,
        SUFFIX = Qt::UserRole+4,
        IS_DIR = Qt::UserRole+5,
        IS_HIDDEN = Qt::UserRole+6
    };
    enum SortField { Unsorted, Name, Time, Size, Type };


    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;


    //Q_INVOKABLE void listFolder(const QString path = "e:/");
    //Q_INVOKABLE void back();

    virtual void classBegin();
    virtual void componentComplete();


signals:
    void folderChanged();
    void countChanged();


private slots:
    QUrl folder() const;
    void setFolder(const QUrl &folder);
    QUrl parentFolder() const;
    QStringList nameFilters() const;
    void setNameFilters(const QStringList &filters);
    FBrowser::SortField sortField() const;
    void setSortField(SortField field);
    bool sortReversed() const;
    void setSortReversed(bool rev);
    bool showDirs() const;
    void setShowDirs(bool on);
    bool showHidden() const;
    void setShowHidden(bool on);
    bool showOnlyDirs() const;
    void setShowOnlyDirs(bool on);

    void handleDataChanged(const QModelIndex &start, const QModelIndex &end);
    void inserted(const QModelIndex &index, int start, int end);
    void removed(const QModelIndex &index, int start, int end);
    void refresh();



private:
    Q_DISABLE_COPY(FBrowser)
    FBrowserPrivate *d;

};

QML_DECLARE_TYPE(FBrowser)

QT_END_HEADER

#endif // FILEBROWSER_H
