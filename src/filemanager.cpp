#include "filemanager.h"

FileManager::FileManager(QObject *parent) :
    QAbstractListModel(parent), m_root("")
{
    QHash<int, QByteArray>roles;
    roles[NAME] = "name";
    roles[BASE_NAME] = "base_name";
    roles[PATH] = "path";
    roles[SIZE] = "size";
    roles[LAST_MODIFIED] = "lastModified";
    roles[LAST_ACCESSED] = "lastAccessed";
    roles[SIZE] = "size";
    roles[IS_DIR] = "isDir";
    roles[IS_HIDDEN] = "isHidden";
    roles[IS_READABLE] = "isReadable";
    roles[IS_WRITABLE] = "isWritable";
    roles[IS_ROOT] = "isRoot";
    roles[SELECTED] = "selected";
    setRoleNames(roles);


    connect(this, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(modelReset()),
            this, SIGNAL(countChanged()));
}

FileManager::~FileManager()
{
    clear();
}


QVariant FileManager::data(const QModelIndex &index, int role) const
{
    if(index.row() < 0 || index.row() >= m_list.size())
        return QVariant();
    switch(role){
    case NAME:
        if (m_current_folder.isEmpty()){
            return m_list.at(index.row())->path();
        }else{
            return m_list.at(index.row())->name();
        }
        //return m_list.at(index.row())->name();
    case BASE_NAME:
        return m_list.at(index.row())->baseName();
    case PATH:
        return m_list.at(index.row())->path();
    case SIZE:
        return m_list.at(index.row())->size();
    case LAST_MODIFIED:
        return m_list.at(index.row())->lastModified();
    case LAST_ACCESSED:
        return m_list.at(index.row())->lastAccessed();
    case IS_DIR:
        return m_list.at(index.row())->isDir();
    case IS_HIDDEN:
        return m_list.at(index.row())->isHidden();
    case IS_READABLE:
        return m_list.at(index.row())->isReadable();
    case IS_WRITABLE:
        return m_list.at(index.row())->isWritable();
    case IS_ROOT:
        return m_list.at(index.row())->isRoot();
    case SELECTED:
        return m_list.at(index.row())->selected();
    default:
        return QVariant();
    }
}

void FileManager::classBegin()
{

}


void FileManager::componentComplete()
{
    m_filter = QDir::AllEntries | QDir::Readable | QDir::NoSymLinks | QDir::NoDotAndDotDot;
#ifdef Q_OS_HARMATTAN
    init("/home/user");
#else
    init();
#endif
}



void FileManager::clear()
{
    qDeleteAll(m_list);
    m_list.clear();
    reset();
}


QString FileManager::folder() const
{
    if (m_current_folder.isEmpty()){
#ifdef Q_OS_HARMATTAN
        return QString("/");
#else
        return QString("Drives");
#endif
    }
    return m_current_folder;
}


void FileManager::setFolder(const QString path)
{
    init(path);
}


bool FileManager::showOnlyDirs() const
{
    if (m_filter == (QDir::AllDirs | QDir::Readable | QDir::NoSymLinks | QDir::NoDotAndDotDot)){
        return true;
    }else{
        return false;
    }
}


void FileManager::setShowOnlyDirs(bool on)
{
    if (on){
        m_filter = QDir::AllDirs | QDir::Readable | QDir::NoSymLinks | QDir::NoDotAndDotDot;
    }else{
        m_filter = QDir::AllEntries | QDir::Readable | QDir::NoSymLinks | QDir::NoDotAndDotDot;
    }
}



void FileManager::init(QString path)
{
    m_current_folder = path;
    emit isRootChanged();
    emit folderChanged();
    clear();
    QFileInfoList list;
    if (path.isEmpty()){
        list = QDir::drives();
    }else{
        list = QDir(m_current_folder).entryInfoList(m_filter, QDir::DirsFirst | QDir::Type);
    }
    if (!list.isEmpty()){
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        foreach(QFileInfo temp, list){
            //qDebug() << temp.fileName() << temp.isRoot();
            FileManagerItem *item = new FileManagerItem(temp, this);
            connect(item, SIGNAL(dataChanged()), this, SLOT(handleDataChanged()));
            m_list << item;


        }
        endInsertRows();
    }
}


QModelIndex FileManager::indexFromItem(FileManagerItem *item)
{
    Q_ASSERT(item);
    for(int row = 0; row < m_list.size(); ++row) {
        if(m_list.at(row) == item) return index(row);
    }
    return QModelIndex();
}


void FileManager::handleDataChanged()
{
    FileManagerItem *item = static_cast<FileManagerItem*>(sender());
    QModelIndex index = indexFromItem(item);
    if (index.isValid())
        emit dataChanged(index, index);
}



void FileManager::back()
{
    if (m_current_folder.isEmpty()){
        return;
    }
    QDir dir(m_current_folder);
    qDebug() << dir.absolutePath();
    if (dir.cdUp()){
        m_current_folder = dir.absolutePath();
    }else{
        m_current_folder = "";
    }
    init(m_current_folder);
}


QObject *FileManager::get(int index) const
{
    if (index < 0 || index >= m_list.size()) {
        return 0;
    }
    return m_list.at(index);
}
