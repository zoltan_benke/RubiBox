#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QAbstractListModel>
#include <QDeclarativeParserStatus>
#include <QFileInfo>
#include <QDir>

#include "filemanageritem.h"

#include <QDebug>

class FileManager : public QAbstractListModel, public QDeclarativeParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QDeclarativeParserStatus)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QString folder READ folder WRITE setFolder NOTIFY folderChanged)
    Q_PROPERTY(bool isRoot READ isRoot NOTIFY isRootChanged)
    Q_PROPERTY(bool showOnlyDirs READ showOnlyDirs WRITE setShowOnlyDirs)
    Q_PROPERTY(QString root READ root WRITE setRoot NOTIFY rootChanged)
public:
    explicit FileManager(QObject *parent = 0);
    virtual ~FileManager();


    enum{
        NAME,
        BASE_NAME,
        PATH,
        SIZE,
        LAST_MODIFIED,
        LAST_ACCESSED,
        IS_DIR,
        IS_HIDDEN,
        IS_READABLE,
        IS_WRITABLE,
        IS_ROOT,
        SELECTED
    };


    inline int rowCount(const QModelIndex &parent = QModelIndex()) const { Q_UNUSED(parent); return m_list.size(); }
    QVariant data(const QModelIndex &index, int role) const;


    virtual void classBegin();
    virtual void componentComplete();


signals:
    void folderChanged();
    void countChanged();
    void isRootChanged();
    void rootChanged();


public slots:
    QObject *get(int index) const;
    void back();
    void clear();


private slots:
    QString folder() const;
    void setFolder(const QString path);
    bool showOnlyDirs() const;
    void setShowOnlyDirs(bool on);
    inline QString root() const { return m_root; }
    inline void setRoot(const QVariant &value){
        m_root = value.toBool();
        emit rootChanged();
        emit isRootChanged();
    }

    inline bool isRoot() const {
        if (m_root.isEmpty()){
#ifdef Q_OS_HARMATTAN
            return (m_current_folder == "/");
#else
            return m_current_folder.isEmpty();
#endif
        }else{
            return (m_current_folder == m_root);
        }
    }
    void init(QString path = "");

    QModelIndex indexFromItem(FileManagerItem* item);
    void handleDataChanged();



private:
    Q_DISABLE_COPY(FileManager)
    QString m_current_folder;
    bool m_isRoot;
    QList<FileManagerItem*>m_list;
    QDir::Filters m_filter;
    QString m_root;
    
};

#endif // FILEMANAGER_H
