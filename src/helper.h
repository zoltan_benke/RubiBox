#ifndef HELPER_H
#define HELPER_H

#include <QObject>
#include <QFeedbackThemeInterface>
#include <QFeedbackEffect>
#include <QFileInfo>
#include <QDir>
#include <QClipboard>
#include <QApplication>
#include "crypt/simplecrypt.h"

QTM_USE_NAMESPACE

const qint64 MAGIC_NUMBER = 0x23081984;

class Helper : public QObject
{
    Q_OBJECT
    Q_ENUMS(QFeedbackEffect::ThemeEffect)
public:
    explicit Helper(QObject *parent = 0);
    virtual ~Helper();
    


    Q_INVOKABLE QString cryptText(QString &text);
    Q_INVOKABLE bool decryptText(QString &crypted, QString &check);

    
public slots:
    QString convSize(const qint64 size);
    void playEffect(const QFeedbackEffect::ThemeEffect &type = QFeedbackEffect::ThemeBasicItem);
    QString getIcon(const bool inverted,
                    const bool isFolder,
                    const int prepend = 1,
                    const bool native = false,
                    const QString name = "");
    qint64 getSize(QUrl path);
    void copy(const QString text);


private slots:
    QString iconFromSuffix(const QString suffix);


private:
    QClipboard *clipBoard;
    SimpleCrypt *crypt;

    
};

#endif // HELPER_H
