#include "boxitem.h"

BoxItem::BoxItem(QVariantMap data, QObject *parent):
    ModelItem(parent), _data(data)
{
    _data.insert("selected", false);
}

QHash<int, QByteArray> BoxItem::roleNames() const
{
    QHash<int, QByteArray>roles;
    roles[IS_DIR] = "isDir";
    roles[ID] = "id";
    roles[SEQUENCE_ID] = "seq_id";
    roles[ETAG] = "etag";
    roles[SHA1] = "sha1";
    roles[NAME] = "name";
    roles[SIZE] = "size";
    roles[SELECTED] = "selected";
    roles[SHARE_LINK] = "share_link";
    roles[SYNCED] = "sync_state";
    return roles;

}


void BoxItem::setProperty(const QVariant &role, const QVariant &value)
{
    QHashIterator<int, QByteArray> hashItr(roleNames());
    while(hashItr.hasNext()){
        hashItr.next();
        if (hashItr.value() == role.toByteArray()){
            _data[hashItr.value()] = value;
            emit dataChanged();
            return;
        }
    }
}


QVariant BoxItem::data(int role) const
{
    switch (role){
    case IS_DIR:
        return isDir();
    case ID:
        return id();
    case SEQUENCE_ID:
        return seq_id();
    case ETAG:
        return etag();
    case SHA1:
        return sha1();
    case NAME:
        return name();
    case SIZE:
        return size();
    case SELECTED:
        return selected();
    case SHARE_LINK:
        return share_link();
    case SYNCED:
        return sync_state();
    default:
        return QVariant();
    }
}
