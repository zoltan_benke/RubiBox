#include "transferitem.h"


TransferItem::TransferItem(const QVariant &id, const QVariant &name, const QVariant &size, QObject *parent) :
    ModelItem(parent)
{
    _name = name;
    _id = id;
    _received_size = 0;
    _size = size;
    _active = false;
    _error = false;
    _downloading = true;
    _speed = "0 b/s";
    _toFolder = "";
    _path = "";
}


TransferItem::TransferItem(const QVariant &path, const QVariant &size, QObject *parent) :
    ModelItem(parent)
{
    QFileInfo in(path.toString());
    _name = in.fileName();
    _size = size;
    _id = "0";
    _active = false;
    _error = false;
    _downloading = false;
    _speed = "0 b/s";
    _received_size = 0;
    _toFolder = "";
    _path = path;
}


QHash<int, QByteArray> TransferItem::roleNames() const
{
    QHash<int, QByteArray>roles;
    roles[NAME] = "name";
    roles[ID] = "id";
    roles[RECEIVED_SIZE] = "received_size";
    roles[SIZE] = "size";
    roles[ACTIVE] = "active";
    roles[ERROR] = "error";
    roles[ERROR_STRING] = "error_string";
    roles[DOWNLOADING] = "download";
    roles[SPEED] = "speed";
    roles[TO_FOLDER] = "to_folder";
    roles[PATH] = "path";
    return roles;
}


QVariant TransferItem::data(int role) const
{
    switch (role){
    case NAME:
        return name();
    case ID:
        return id();
    case RECEIVED_SIZE:
        return received_size();
    case SIZE:
        return size();
    case ACTIVE:
        return active();
    case ERROR:
        return error();
    case DOWNLOADING:
        return downloading();
    case SPEED:
        return speed();
    case TO_FOLDER:
        return toFolder();
    case PATH:
        return path();
    default:
        return QVariant();
    }
}



void TransferItem::setReceivedSize(const qint64 &received_size)
{
    if (_received_size != received_size){
        _received_size = received_size;
        emit dataChanged();
    }
}

void TransferItem::setActive(const bool &active)
{
    if (_active != active){
        _active = active;
        emit dataChanged();
    }
}

void TransferItem::setDownloading(const bool &downloading)
{
    if (_downloading != downloading){
        _downloading = downloading;
        emit dataChanged();
    }
}

void TransferItem::setToFolder(const QString &toFolder)
{
    if (_toFolder != toFolder){
        _toFolder = toFolder;
        emit dataChanged();
    }
}

void TransferItem::setSpeed(const QString &speed)
{
    _speed = speed;
    emit dataChanged();
}
