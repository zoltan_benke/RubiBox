#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QAbstractListModel>


Q_DECLARE_METATYPE(QModelIndex)

class ModelItem: public QObject
{
    Q_OBJECT
public:
    ModelItem(QObject* parent = 0) : QObject(parent) {}
    virtual ~ModelItem() {}


    virtual QVariant id() const = 0;
    virtual QVariant data(int role) const = 0;
    virtual QHash<int, QByteArray> roleNames() const = 0;

public slots:
    virtual void setProperty(const QVariant &role, const QVariant &value) = 0;

signals:
    void dataChanged();

};


class Model : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
public:
    explicit Model(ModelItem *prototype, QObject *parent = 0);
    virtual ~Model();


    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;


signals:
    void countChanged();
    

public slots:
    /** For section scroller **/
    QVariant get(int index);
    ModelItem* getItem(int index);
    void appendItem(ModelItem* item);
    void appendItems(const QList<ModelItem*> &items);
    void insertItem(int index, ModelItem *item);
    void setProperty(const int index, const QVariant &role, const QVariant &value);
    void removeItem(int index);
    bool itemExists(const QVariant &id);
    void clear();



private slots:
    QModelIndex indexFromItem(ModelItem *item);
    void handleDataChanged();
    


private:
    ModelItem* m_prototype;
    QList<ModelItem*>_list;
};

#endif // MODEL_H
