#ifndef SORTMODEL_H
#define SORTMODEL_H

#include <QSortFilterProxyModel>
#include "model.h"
#include "items/boxitem.h"

class SortModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QString sortRole READ sortRole WRITE setSortRole NOTIFY sortRoleChanged)
    Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder WRITE setSortOrder)
public:
    explicit SortModel(QObject *parent = 0);
    
    static SortModel *instance();

    inline int count() const { return QSortFilterProxyModel::rowCount(); }

signals:
    void countChanged();
    void sortRoleChanged();
    

public slots:
    void setModel(QObject *source);
    void reSort();
    QVariant get(int index);
    ModelItem* getItem(int index);
    void appendItem(ModelItem* item);
    void insertItem(int index, ModelItem *item);
    void setProperty(const int index, const QVariant &role, const QVariant &value);
    void removeItem(int index);
    bool itemExists(const QVariant &id);
    void clear();
    

protected slots:
    void syncRoleNames();

    void setSortRole(const QString &role);
    QString sortRole() const;

    void setSortOrder(const Qt::SortOrder order);


private:
    static SortModel *_instance;
    QString _sortRole;
    QString _sortBy;
    QHash<QString, int> _roleNames;

};

#endif // SORTMODEL_H
