#ifndef SESSION_H
#define SESSION_H

#include <QObject>


#include "storage/database.h"
#include "storage/settings.h"
#include "boxapi.h"
#include "helper.h"

#include "model/model.h"
#include "model/items/boxitem.h"
#include "model/sortmodel.h"

class Session : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(QString parentHeader READ parentHeader NOTIFY headerChanged)
    Q_PROPERTY(QString currentHeader READ currentHeader NOTIFY headerChanged)
    Q_PROPERTY(QString currentFolder READ currentFolder NOTIFY currentFolderChanged) // Hold current folder ID
    Q_PROPERTY(QString parentFolder READ parentFolder NOTIFY parentFolderChanged) // Hold parent folder ID
    Q_PROPERTY(bool isRoot READ isRoot NOTIFY currentFolderChanged) // Hold isRoot property
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged) // Helper for current selected item of Model
    Q_PROPERTY(QString action READ action WRITE setAction NOTIFY actionChanged)
public:
    Session(QObject *parent = 0);
    virtual ~Session();


    BoxApi *boxApi;
    Database *db;
    Settings *settings;
    Model *bModel;
    SortModel *bModelSort;
    Helper *helper;
    Model *tModel;




signals:
    void loadingChanged();
    void headerChanged();
    void currentFolderChanged();
    void parentFolderChanged();
    void currentIndexChanged();
    void actionChanged();
    void cmItemIdChanged();
    void allItemsLoaded();

    

public slots:
    void getAuthToken(const QVariant code);
    void renewToken();
    void getTicket();
    void listFolder(const QVariant folderId = "0", const int offset = 0);
    void createFolder(const QVariant name);
    void renameItem(const QVariant newName);
    void removeItem();
    void copyItem(const QVariant name = QVariant());
    void moveItem(const QVariant name = QVariant());
    void syncState(const QVariant id, const QVariant sync_state);
    void userInfo();
    void share();


    void downloadItem(const QVariant id, const QVariant name, const QVariant size);
    void uploadItem(const QString &path, const QString &size);

    bool isSelected() const;
    void downloadSelected();


private slots:
    void prepareApi();
    inline QString parentHeader() const { return m_header_parent; }
    inline QString currentHeader() const { return m_header_current; }
    void setHeaderString(QString parent, QString current);

    inline int currentIndex() const { return m_currentIndex; }
    inline void setCurrentIndex(const QVariant index){
        if (m_currentIndex != index.toInt()){
            m_currentIndex = index.toInt();
            emit currentIndexChanged();
        }
    }
    /** Is ROOT property **/
    bool isRoot() const { return m_current_folder_id == "0"; }

    /** Sets Current/Parent folder ID **/
    inline QString currentFolder() const { return m_current_folder_id; }
    inline void setCurrentFolder(const QVariant currentFolderId){
        m_current_folder_id = currentFolderId.toString();
        emit currentFolderChanged();
    }

    inline QString parentFolder() const { return m_parent_folder_id; }
    inline void setParentFolder(const QVariant parentFolderId){
        m_parent_folder_id = parentFolderId.toString();
        emit parentFolderChanged();
    }

    inline QString action() const { return m_actionType; }
    inline void setAction(const QVariant action){
        if (action.toString() == "copy" || action.toString() == "move"){
            _cmTemp["isDir"] = ((BoxItem*)bModelSort->getItem(m_currentIndex))->isDir();
            _cmTemp["id"] = ((BoxItem*)bModelSort->getItem(m_currentIndex))->id();
        }
        m_actionType = action.toString();
        emit actionChanged();
    }

    inline bool loading() const { return m_loading; }
    inline void reqStart() {
        if (!m_loading){
            m_loading = true;
            emit loadingChanged();
        }
    }
    inline void reqFinish() {
        if (m_loading){
            m_loading = false;
            emit loadingChanged();
        }
    }
    void uploadItemDone(const QString toFolder, const QVariantMap item);



private:
    bool m_loading;
    QString m_header_parent, m_header_current,
    m_current_folder_id, m_parent_folder_id;
    int m_currentIndex;
    QString m_actionType, m_cmItemId;
    QVariantMap _cmTemp;
};

#endif // SESSION_H
