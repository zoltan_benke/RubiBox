#include "database.h"


Database *Database::_instance = 0;

Database::Database(QObject *parent) :
    QObject(parent)
{
    QString path;
    m_database = QSqlDatabase::addDatabase("QSQLITE");
#ifdef Q_OS_HARMATTAN
    QDir dir;
    dir.mkpath("/opt/RubiBox/database");
    path.append("/opt/RubiBox/database");
    m_database.setDatabaseName(QString("%1/rubibox.db").arg(path));
#elif defined(Q_OS_SYMBIAN)
    path.append(qApp->applicationDirPath());
    path.append(QDir::separator()).append("rubibox.db");
    path = QDir::toNativeSeparators(path);
    m_database.setDatabaseName(path);
#else
    QDir dir;
    dir.mkpath("../RubiBox/database/");
    path.append("../RubiBox/database/rubibox.db");
    m_database.setDatabaseName(path);
#endif

#ifdef QT_DEBUG
    qDebug() << "DATABASE PATH:" << path;
#endif
    initialize();
}

Database::~Database()
{
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO << "DESTRUCTOR";
#endif

}


Database *Database::instance()
{
    if (!_instance)
        _instance = new Database;
    return _instance;
}


void Database::initialize()
{
    if (m_database.open()){
        m_database.exec("CREATE TABLE IF NOT EXISTS accounts (name TEXT UNIQUE, token TEXT, refresh_token TEXT, active INTEGER)");
        m_database.exec("CREATE TABLE IF NOT EXISTS usePassword (use TEXT UNIQUE)");
        m_database.exec("CREATE TABLE IF NOT EXISTS password (password TEXT UNIQUE)");
    }
    else{
#ifdef QT_DEBUG
        qDebug() << m_database.lastError();
#endif
    }
}


QByteArray Database::getToken()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT token FROM accounts WHERE active = 1");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get token from DevPDA";
#endif
    m_database.close();
    return value.toString().toUtf8();
}



QByteArray Database::getRefreshToken()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT refresh_token FROM accounts WHERE active = 1");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get refresh token from DevPDA";
#endif
    m_database.close();
    return value.toString().toUtf8();
}


bool Database::clearAllAccounts()
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;

    bool success = query.exec("DELETE FROM accounts");

    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABEL TO DELETE accounts";
#endif
    }
    return success;
}



bool Database::clearUnusedAccounts()
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;

    bool success = query.exec("DELETE FROM accounts WHERE token IS NULL");

    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABLE DELETE accounts WHERE token IS NULL";
#endif
    }else{
#ifdef QT_DEBUG
        qDebug() << "Unused accounts cleared";
#endif
    }
    m_database.close();
    return success;
}



bool Database::authorized()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT token FROM accounts WHERE active = 1");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }
    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get authorized from accounts";
#endif
    m_database.close();
    if (value.isNull()){
        return false;
    }
    return true;
}


QString Database::active_account()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT name FROM accounts WHERE active = 1");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }
    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get name from accounts";
#endif
    m_database.close();
    return value.toString();
}



int Database::accountsCount()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT count(*) FROM accounts WHERE token IS NOT NULL");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }
    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get count from accounts";
#endif
    m_database.close();
    return value.toInt();
}


QList<QVariant> Database::accounts()
{
    QList<QVariant> temp;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;

    bool success = query.exec(QString("SELECT name, active FROM accounts WHERE token IS NOT NULL"));
    while (query.next()){
        if (query.value(1).toInt() != 1){
            temp.append(query.value(0));
        }
    }
    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABLE get JSON data FROM accounts";
#endif
    }
    return temp;
}



QString Database::password()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT * FROM password");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }
    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get pass from password";
#endif
    m_database.close();
    return value.toString();
}


bool Database::usePassword()
{
    QVariant value;
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("SELECT use FROM usePassword");

    bool success = query.exec();
    while (query.next()){
        value = query.value(0);
    }
    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE get use from usePassword";
#endif
    m_database.close();
    if (value.isNull()){
        return false;
    }
    return value.toBool();
}


bool Database::setUsePassword(const QVariant &usePassword)
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.exec("DELETE FROM usePassword");
    query.prepare("INSERT OR REPLACE INTO usePassword VALUES(?)");
    query.addBindValue(usePassword);

    bool success = query.exec();
    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABLE set UsePass to password";
#endif
        return success;
    }
    m_database.close();
    emit usePasswordChanged();
    return success;
}



bool Database::setPassword(const QVariant &pass)
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.exec("DELETE FROM password");


    query.prepare("INSERT OR REPLACE INTO password VALUES(?)");
    query.addBindValue(pass);

    bool success = query.exec();

    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABLE INSERT INTO password (pass)";
#endif
        return success;
    }
    m_database.close();
    emit passwordChanged();
    return success;
}



bool Database::addAccount(const QVariant &accountName, const int &active)
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE INTO accounts (name, active) VALUES(?, ?)");
    query.addBindValue(accountName.toString());
    query.addBindValue(active);

    bool success = query.exec();

    if (!success)
#ifdef QT_DEBUG
        qWarning() << "UNABLE INSERT INTO accounts (name, active)";
#endif
    m_database.close();
    return success;
}


bool Database::deleteAccount(const QVariant &accountName)
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("DELETE FROM accounts WHERE name = ?");
    query.addBindValue(accountName);

    bool success = query.exec();

    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABLE DELETE account";
#endif
    }else{
        emit accountsCountChanged();
        emit accountsChanged();
    }
    m_database.close();
    return success;
}



bool Database::setToken(const QVariant &accountName, const QVariant &token, const QVariant &refresh_token)
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.prepare("UPDATE accounts SET token = ?, refresh_token = ? WHERE name = ?");
    query.addBindValue(token);
    query.addBindValue(refresh_token);
    query.addBindValue(accountName);

    bool success = query.exec();

    if (!success){
#ifdef QT_DEBUG
        qWarning() << "UNABLE SET token, refresh_token INTO accounts "+accountName.toString();
#endif
    }
    else{
        emit tokenChanged();
        emit accountsChanged();
        emit accountsCountChanged();
    }
    m_database.close();
    return success;
}


bool Database::setAsActive(const QVariant &accountName)
{
    if (!m_database.isOpen())
        m_database.open();
    QSqlQuery query;
    query.exec("SELECT * FROM accounts");


    while (query.next()){
        query.exec("UPDATE accounts SET active = 0");
    }

    query.prepare("UPDATE accounts SET active = 1 WHERE name = ?");
    query.addBindValue(accountName);

    bool success = query.exec();

    if (!success){
#ifdef QT_DEBUG
        qWarning() << "Unable set as active";
#endif
    }else{
        emit activeAccountChanged();
    }
    m_database.close();
    return success;
}
